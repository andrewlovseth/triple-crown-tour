<?php get_header(); ?>

	<section id="generic-page">
		<div class="wrapper">
			
			<article class="default">

				<div class="article-header">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="article-body">
					<?php the_content(); ?>
				</div>

			</article>

		</div>
	</section>

<?php get_footer(); ?>