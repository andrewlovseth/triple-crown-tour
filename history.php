<?php

/*

	Template Name: History

*/

get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>Triple Crown Tour</span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>

				<?php if(have_rows('division_history')): while(have_rows('division_history')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'history' ): ?>

				    	<div class="division">

							<h3><?php the_sub_field('division_label'); ?></h3>

							<table>
								<thead>
								    <tr>
								        <td class="year">Year</td>
								        <td class="champion">Champion</td>
								        <td class="runner-up">Runner-up</td>
								        <td class="location">Location</td>
								    </tr>
								</thead>

								<tbody>
									<?php if(have_rows('years')): while(have_rows('years')): the_row(); ?>
									 
									    <tr>
									        <td class="year"><?php the_sub_field('year'); ?></td>
									        <td class="champion"><?php the_sub_field('champion'); ?></td>
									        <td class="runner-up"><?php the_sub_field('runner_up'); ?></td>
									        <td class="location"><?php the_sub_field('location'); ?></td>
									    </tr>

									<?php endwhile; endif; ?>
								</tbody>					
							</table>

						</div>

					
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>

			</article>

		</div>
	</section>

<?php get_footer(); ?>