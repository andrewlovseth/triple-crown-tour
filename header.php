<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i|Open+Sans+Condensed:300,700|Roboto+Condensed:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo the_slug();?>">

	<section id="utility-nav">
		<div class="wrapper">

			<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>

			<div class="events section">
				<strong>Events</strong>
				<a href="<?php echo site_url('/us-open/'); ?>">U.S. Open</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/pro-championships/'); ?>">Pro Championships</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/national-championships/'); ?>">National Championships</a>
			</div>

			<div class="divisions section">
				<strong>Divisions</strong>
				<a href="<?php echo site_url('/men/'); ?>">Men</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/mixed/'); ?>">Mixed</a>
				<span class="dot">&middot;</span>
				<a href="<?php echo site_url('/women/'); ?>">Women</a>
			</div>

		</div>
	</section>


	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<h1><a href="<?php echo site_url('/'); ?>">Triple Crown Tour</a></h1>

			</div>

			<?php if(get_field('show_watch_live', 'options') == true): ?>
				<a href="<?php echo site_url('/watch-live'); ?>" class="watch-live">Watch Live</a>
			<?php endif; ?>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>

	<?php get_template_part('partials/nav-bar'); ?>

	<?php get_template_part('partials/nav-dropdown'); ?>

