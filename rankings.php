<?php

/*

	Template Name: Rankings

*/


get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $parentID = wp_get_post_parent_id($post->ID); the_field('division_name', $parentID); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<div class="update">
				<em>Last updated: <?php the_field('last_updated'); ?></em>
			</div>

			<?php the_field('rankings'); ?>

			<div class="full-rankings">
				<a href="<?php the_field('full_rankings_link'); ?>" class="btn" rel="external">View Full Rankings</a>
			</div>

		</div>
	</section>

<?php get_footer(); ?>