<?php

/*

	Template Name: Awards

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>

				<h3>Past Award Winners</h3>

				<?php if(have_rows('mens_awards')): ?>

					<div class="division men">

						<h4>Men's Division</h4>
				 
						<table>
							<thead>
							    <tr>
							        <td class="year">Year</td>
							        <td class="individual-spirit">Farricker Spirit Award</td>
							        <td class="team-spirit">Team Spirt Award</td>
							    </tr>
							</thead>

							<tbody>
								<?php while(have_rows('mens_awards')): the_row(); ?>
								 
								    <tr>
								        <td class="year"><?php the_sub_field('year'); ?></td>
								        <td class="individual-spirit"><?php the_sub_field('individual_spirit'); ?></td>
								        <td class="team-spirit"><?php the_sub_field('team_spirit'); ?></td>
								    </tr>

								<?php endwhile; ?>
							</tbody>					
						</table>

					</div>

				<?php endif; ?>


				<?php if(have_rows('mixed_awards')): ?>

					<div class="division men">

						<h4>Mixed Division</h4>
				 
						<table>
							<thead>
							    <tr>
							        <td class="year">Year</td>
							        <td class="individual-spirit">Spirit and Equity Award</td>
							        <td class="team-spirit">Team Spirt Award</td>
							    </tr>
							</thead>

							<tbody>
								<?php while(have_rows('mixed_awards')): the_row(); ?>
								 
								    <tr>
								        <td class="year"><?php the_sub_field('year'); ?></td>
								        <td class="individual-spirit"><?php the_sub_field('individual_spirit'); ?></td>
								        <td class="team-spirit"><?php the_sub_field('team_spirit'); ?></td>
								    </tr>

								<?php endwhile; ?>
							</tbody>					
						</table>

					</div>

				<?php endif; ?>


				<?php if(have_rows('womens_awards')): ?>

					<div class="division women">

						<h4>Women's Division</h4>
				 
						<table>
							<thead>
							    <tr>
							        <td class="year">Year</td>
							        <td class="individual-spirit">Kathy Pufahl Spirit Award</td>
							        <td class="team-spirit">Team Spirt Award</td>
							    </tr>
							</thead>

							<tbody>
								<?php while(have_rows('womens_awards')): the_row(); ?>
								 
								    <tr>
								        <td class="year"><?php the_sub_field('year'); ?></td>
								        <td class="individual-spirit"><?php the_sub_field('individual_spirit'); ?></td>
								        <td class="team-spirit"><?php the_sub_field('team_spirit'); ?></td>
								    </tr>

								<?php endwhile; ?>
							</tbody>					
						</table>

					</div>

				<?php endif; ?>

			</article>

		</div>
	</section>

<?php get_footer(); ?>