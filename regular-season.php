<?php

/*

	Template Name: Regular Season

*/

get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $parentID = wp_get_post_parent_id($post->ID); the_field('division_name', $parentID); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>

				<?php if(have_rows('tournaments')): while(have_rows('tournaments')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'tournament' ): ?>

				    	<div class="tournament">

							<h3><?php the_sub_field('name'); ?></h3>
							<h4><?php the_sub_field('date'); ?> | <?php the_sub_field('location'); ?></h4>

							<?php if(get_sub_field('champion')): ?>
								<h5><strong>Champion:</strong> <?php the_sub_field('champion'); ?></h5>
							<?php endif; ?>

							<?php if(get_sub_field('score_reporter_link')): ?>
								<a href="<?php the_sub_field('score_reporter_link'); ?>" rel="external" class="btn">Score Reporter</a>
							<?php endif; ?>


						</div>

					
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>





			</article>

		</div>
	</section>

<?php get_footer(); ?>