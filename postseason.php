<?php

/*

	Template Name: Postseason

*/

get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $parentID = wp_get_post_parent_id($post->ID); the_field('division_name', $parentID); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<?php if(have_rows('sectionals')): ?>

					<section id="sectionals" class="round">

						<h2 class="round-name">
							Sectionals.<br/>
							<em><?php the_field('sectionals_number_of_tournaments'); ?></em> Tournaments.<br/>
							<em><?php the_field('sectionals_number_of_teams'); ?></em> Teams.
						</h2>

						<div class="round-nav">
							<?php if(have_rows('sectionals')): while(have_rows('sectionals')) : the_row(); ?>
							 
							 		<?php if( get_row_layout() == 'region' ): ?>
										
										<a href="#sectionals-<?php echo sanitize_title_with_dashes(get_sub_field('region_name')); ?>">
											<?php the_sub_field('region_name'); ?>
										</a>
										
								    <?php endif; ?>
							 
							<?php endwhile; endif; ?>
						</div>
						
						<?php while(have_rows('sectionals')) : the_row(); ?>

						    <?php if( get_row_layout() == 'region' ): ?>

						    	<div class="region" id="sectionals-<?php echo sanitize_title_with_dashes(get_sub_field('region_name')); ?>">
						    		
						    		<h3 class="region-name"><?php the_sub_field('region_name'); ?></h3>

						    		<?php if(have_rows('sectional_tournaments')): ?>

						    			<div class="sections">
						    		

							    			<?php while(have_rows('sectional_tournaments')): the_row(); ?>
										 
											    <div class="section">
													<h3><?php the_sub_field('name'); ?></h3>
													<h4><?php the_sub_field('date'); ?> | <?php the_sub_field('location'); ?></h4>

													<?php if(get_sub_field('champion')): ?>
														<h5><strong>Champion:</strong> <?php the_sub_field('champion'); ?></h5>
													<?php endif; ?>

													<?php if(get_sub_field('score_reporter_link')): ?>
														<a href="<?php the_sub_field('score_reporter_link'); ?>" rel="external" class="btn">Score Reporter</a>
													<?php endif; ?>
											    </div>

											<?php endwhile; ?>

										</div>

									<?php endif; ?>

								</div>
							
						    <?php endif; ?>

					    <?php endwhile; ?>
					 
					</section>
				
				<?php endif; ?>


				<?php if(have_rows('regionals')): ?>

					<section id="regionals" class="round">

						<h2 class="round-name">
							Regionals.<br/>
							<em><?php the_field('regionals_number_of_tournaments'); ?></em> Tournaments.<br/>
							<em><?php the_field('regionals_number_of_teams'); ?></em> Teams.
						</h2>

						<div class="round-nav">
							<?php if(have_rows('regionals')): while(have_rows('regionals')) : the_row(); ?>
							 
						 		<?php if( get_row_layout() == 'region' ): ?>
									
									<a href="#regionals-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">
										<?php the_sub_field('name'); ?>
									</a>
									
							    <?php endif; ?>
							 
							<?php endwhile; endif; ?>
						</div>						


						<?php while(have_rows('regionals')) : the_row(); ?>

						    <?php if( get_row_layout() == 'region' ): ?>

						    	<div class="region" id="regionals-<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">
							 		
									<h3><?php the_sub_field('name'); ?></h3>
									<h4><?php the_sub_field('date'); ?> | <?php the_sub_field('location'); ?></h4>

									<?php if(get_sub_field('champion')): ?>
										<h5><strong>Champion:</strong> <?php the_sub_field('champion'); ?></h5>
									<?php endif; ?>

									<?php if(get_sub_field('score_reporter_link')): ?>
										<a href="<?php the_sub_field('score_reporter_link'); ?>" rel="external" class="btn">Score Reporter</a>
									<?php endif; ?>

								</div>
							
						    <?php endif; ?>

					    <?php endwhile; ?>
					 
					</section>
				
				<?php endif; ?>


				<section id="nationals" class="round">
					
			    	<div class="region">

						<h2 class="round-name">
							Nationals.<br/>
							<em><?php the_field('nationals_number_of_teams'); ?></em> Teams<br/>
							<em>1</em> Champion.
						</h2>
						
				 		<h4><?php the_field('nationals_date'); ?> | <?php the_field('nationals_location'); ?></h4>

						<?php if(get_field('nationals_score_reporter_link')): ?>
							<a href="<?php the_field('nationals_score_reporter_link'); ?>" rel="external" class="btn">Score Reporter</a>
						<?php endif; ?>

					</div>

				</section>
			

			</article>

		</div>
	</section>

<?php get_footer(); ?>