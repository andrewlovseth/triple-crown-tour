<?php

/*

	Template Name: Photo Index

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>


	<section id="main">
		<div class="wrapper">

			<section id="galleries">

				<?php
					$args = array(
						'post_type' => 'any',
					    'post_parent' => $post->ID,                               
					    'posts_per_page'    => -1

					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="photo-essay">

						<div class="image">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $photoEssay = get_field('photo_essay'); $image = $photoEssay[0]['photo']; echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<p><?php echo count( get_field('photo_essay') ); ?> Photos</p>

						</div>

					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</section>

		</div>
	</section>

<?php get_footer(); ?>