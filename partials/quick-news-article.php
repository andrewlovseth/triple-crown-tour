<article>
	<h5 class="date"><?php the_time('m/d/Y'); ?></h5>
	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
</article>