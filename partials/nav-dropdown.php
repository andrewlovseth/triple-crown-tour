<nav id="dropdown">
	<div class="wrapper">

		<?php get_template_part('partials/nav-links'); ?>

		<div class="col news">
			<h4>
				<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>
			</h4>
		</div>

	</div>
</nav>