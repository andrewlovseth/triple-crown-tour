<?php if(strpos($_SERVER['REQUEST_URI'], '/us-open/') !== false):?>

	<?php $page = get_page_by_path( 'us-open' ); ?>

<?php elseif(strpos($_SERVER['REQUEST_URI'], '/pro-championships/') !== false):?>

	<?php $page = get_page_by_path( 'pro-championships' ); ?>

<?php elseif(strpos($_SERVER['REQUEST_URI'], '/national-championships/') !== false):?>

	<?php $page = get_page_by_path( 'national-championships' ); ?>

<?php
	endif;
	$event = $page->ID;
?>

<?php if(have_rows('sponsors', $event)): ?>

	<section id="sponsors">
		<div class="wrapper">

			<h2>Sponsors</h2>

			<div id="sponsors-wrapper">

				<?php while(have_rows('sponsors', $event)): the_row(); ?>

					<?php $image = get_sub_field('logo'); ?>

					<a href="<?php the_sub_field('link'); ?>" rel="external" class="<?php echo $image['alt']; ?>">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>

				<?php endwhile; ?>

			</div>

		</div>

	</section>
<?php endif; ?>