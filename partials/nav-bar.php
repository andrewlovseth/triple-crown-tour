<?php if ( is_tree(929) || (is_team() && in_team_category(929)) ): ?>

	<?php $division = 929; // US Open ?>

<?php elseif ( is_tree(953) || (is_team() && in_team_category(953)) ): ?>
	
	<?php $division = 953; // Pro Championships ?>

<?php elseif ( is_tree(968) || (is_team() && in_team_category(968)) ): ?>
	
	<?php $division = 968; // National Championships ?>

<?php endif; ?>


<?php if($division): ?>

	<nav id="bar">
		<div class="wrapper">
			
			<h1><a href="<?php the_field('event_homepage', $division); ?>"><?php the_field('event_name', $division); ?></a></h1>

			<a href="#" id="menu-toggle">Menu</a>

			<div class="menu">
				<?php $main_nav_links = get_field('event_nav_bar', $division); foreach( $main_nav_links as $main_nav_link ):  ?>
				
					<a href="<?php echo get_permalink($main_nav_link); ?>">
						<?php if(get_field('alt_nav_label', $main_nav_link)  && $division != 968): // National Championships ?>
							<?php the_field('alt_nav_label', $main_nav_link); ?>
						<?php else: ?>
							<?php echo get_the_title($main_nav_link); ?>
						<?php endif; ?>
			    	</a>

				<?php endforeach; ?>

				<div class="mobile subnav">
					<?php $sub_nav_links = get_field('event_nav_sub_bar', $division); foreach( $sub_nav_links as $sub_nav_link ):  ?>
					
						<a href="<?php echo get_permalink($sub_nav_link); ?>">
							<?php if(get_field('alt_nav_label', $sub_nav_link)): ?>
								<?php the_field('alt_nav_label', $sub_nav_link); ?>
							<?php else: ?>
								<?php echo get_the_title($sub_nav_link); ?>
							<?php endif; ?>
				    	</a>

					<?php endforeach; ?>
				</div>

			</div>

		</div>
	</nav>

	<nav id="sub-bar">
		<div class="wrapper">

			<?php $sub_nav_links = get_field('event_nav_sub_bar', $division); foreach( $sub_nav_links as $sub_nav_link ):  ?>
			
				<a href="<?php echo get_permalink($sub_nav_link); ?>">
					<?php if(get_field('alt_nav_label', $sub_nav_link)): ?>
						<?php the_field('alt_nav_label', $sub_nav_link); ?>
					<?php else: ?>
						<?php echo get_the_title($sub_nav_link); ?>
					<?php endif; ?>
		    	</a>

			<?php endforeach; ?>

		</div>
	</nav>

<?php endif; ?>




