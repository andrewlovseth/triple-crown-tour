<div class="col">
	<h4>Events</h4>

	<ul>
		<li>
			<a href="<?php echo site_url('/us-open/'); ?>" class="division">U.S. Open</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/us-open/men/'); ?>" class="sub-link">Men</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/us-open/mixed/'); ?>" class="sub-link">Mixed</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/us-open/women/'); ?>" class="sub-link">Women</a>
		</li>

		<li>
			<a href="<?php echo site_url('/pro-championships/'); ?>" class="division">Pro Championships</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/pro-championships/men/'); ?>" class="sub-link">Men</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/pro-championships/mixed/'); ?>" class="sub-link">Mixed</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/pro-championships/women/'); ?>" class="sub-link">Women</a>
		</li>


		<li>
			<a href="<?php echo site_url('/national-championships/'); ?>" class="division">National Championships</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/men/'); ?>" class="sub-link">Men</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/mixed/'); ?>" class="sub-link">Mixed</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/women/'); ?>" class="sub-link">Women</a>
		</li>						
	</ul>

</div>

<div class="col">
	<h4>Divisions</h4>

	<ul>
		<li>
			<a href="<?php echo site_url('/men/'); ?>" class="division">Men</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/men/regular-season/'); ?>" class="sub-link">Regular Season</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/men/postseason/'); ?>" class="sub-link">Postseason</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/men/'); ?>" class="sub-link">National Championships</a>
		</li>

		<li>
			<a href="<?php echo site_url('/mixed/'); ?>" class="division">Mixed</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/mixed/regular-season/'); ?>" class="sub-link">Regular Season</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/mixed/postseason/'); ?>" class="sub-link">Postseason</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/mixed/'); ?>" class="sub-link">National Championships</a>
		</li>

		<li>
			<a href="<?php echo site_url('/women/'); ?>" class="division">Women</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/women/regular-season/'); ?>" class="sub-link">Regular Season</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/women/postseason/'); ?>" class="sub-link">Postseason</a>
			<span class="sep">&middot;</span>
			<a href="<?php echo site_url('/national-championships/women/'); ?>" class="sub-link">National Championships</a>
		</li>					
	</ul>

</div>