<a href="<?php the_permalink(); ?>">
	<?php the_field('team_name'); ?>

	<?php if(get_field($parent_slug . '_seed')): ?>
		(<?php echo get_field($parent_slug . '_seed'); ?>)
	<?php endif; ?>
</a>