<?php
	$post_data = get_post($post->post_parent);
	$parent_slug = $post_data->post_name;
?>

<?php foreach( $posts as $post): setup_postdata($post); ?>

	<?php if(get_field('alt_nav_label') && $parent_slug != 'national-championships'): ?>
		<a href="<?php the_permalink(); ?>"><?php the_field('alt_nav_label'); ?></a>
	<?php else: ?>
    	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php endif; ?>

<?php endforeach; ?>