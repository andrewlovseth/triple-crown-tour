<?php

/*

	Template Name: Photo Essay

*/

get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>Photos</span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<section id="photo-essay">


				<?php if(have_rows('photo_essay')): while(have_rows('photo_essay')): the_row(); ?>
				 
				    <div class="photo">
				    	<div class="image">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

				    		<?php if(get_sub_field('credit')): ?>
					    		<div class="credit">
				    				<span>Photo: <?php the_sub_field('credit'); ?></span>
				    			</div>
				    		<?php endif; ?>

				    	</div>

						<?php if(get_sub_field('caption')): ?>
				    		<div class="caption">
								<?php the_sub_field('caption'); ?>
							</div>
						<?php endif; ?>
				    </div>

				<?php endwhile; endif; ?>

			</section>

		</div>
	</section>

<?php get_footer(); ?>