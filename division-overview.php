<?php

/*

	Template Name: Division Overview

*/


get_header(); ?>


	<?php
		$event = get_post_type(get_the_ID());
		$overviewPage = get_the_ID();
	?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>Triple Crown Tour</span>
				</h2>
				<h1>
					<span><?php the_field('division_name'); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article>
				<div class="about">
					<?php the_field('about_info'); ?>
				</div>


				<?php if(have_rows('faqs')): ?>

					<div class="faqs">
						<h2>FAQs</h2>

						<?php while(have_rows('faqs')): the_row(); ?>

							<div class="faq">
								<a href="#" class="question"><?php the_sub_field('question'); ?></a>
								<div class="answer">
							        <?php the_sub_field('answer'); ?>
							    </div>
							</div>

						<?php endwhile; ?>
					</div>
	
				<?php endif; ?>

			</article>


			<div class="aside-wrapper">

				<?php
					$newsCats = get_field('news_categories');

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 4,
						'category__in' => $newsCats

					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : ?>

					<aside id="latest-news">
						<h3>Latest News</h3>

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>

							<?php get_template_part('partials/quick-news-article'); ?>

						<?php endwhile; ?>

						<a href="<?php echo site_url('/' . get_post_field('post_name', $overviewPage) . '/news/'); ?>" class="btn">All News</a>

					</aside>

				<?php endif; wp_reset_postdata(); ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>